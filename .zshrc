# Path to your oh-my-zsh installation.
  export ZSH="/home/azazel/.oh-my-zsh"

ZSH_THEME="classyTouch"

 ENABLE_CORRECTION="true"
 COMPLETION_WAITING_DOTS="true"

# plugins
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# PATH
export PATH="$HOME/.local/bin:$HOME/.npm-global/bin:$PATH"

# aliases
alias scaur='python ~/.scaur/scaur.py'
alias nf='neofetch --ascii ~/scripts/customnf.txt'
alias ss='~/scripts/rofimgur.sh'
alias update='~/scripts/update'
alias v='nvim'

